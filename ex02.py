

from joblib import Parallel, delayed


def task(i):
    print(i)


Parallel(n_jobs=8)(delayed(task)(i) for i in range(8))
