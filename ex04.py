import os

from joblib import Parallel, delayed


def task1(i):
    os.system(f"echo Sample {i}, Task1")


def task2(i):
    os.system(f"echo Sample {i}, Task2")


def task3(i):
    os.system(f"echo Sample {i}, Task3")


def task4(i):
    os.system(f"echo Sample {i}, Task4")


def task5(i):
    os.system(f"echo Sample {i}, Task5")


def task6(i):
    os.system(f"echo Sample {i}, Task6")


def task7(i):
    os.system(f"echo Sample {i}, Task7")


def task8(i):
    os.system(f"echo Sample {i}, Task8")


def alltask1():
    os.system(f"echo All Task 1")


def alltask2():
    os.system(f"echo All Task 2")


def alltask3():
    os.system(f"echo All Task 3")


pipeline = [ alltask1,
             alltask2,
             [task1,
              task2,
              task3,
              task4,
              task5,
              task6,
              task7,
              task8],
            alltask3]


for task in pipeline:
    if type(task) is list:
        for sample in range(6):
            print()
            print(f"Starting sample {sample}")
            for subtask in task:
                subtask(sample)
            print(f"Finished sample {sample}")
    else:
        task()

