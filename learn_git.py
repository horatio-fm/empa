def main():

    rename_branch()
    update_from_remote_branch()

    def subgroup():
        update_remote_branch()
        delete_branch()


def delete_branch():
    """
    git branch --delete experimental_idea

    git branch --delete restoring_old_commit
    delete it from the remote rep
    git push --delete restoring_old_commit

    # delete all local git branches that have been merged
    git branch --merged | grep -v "\*" | xargs -n 1 git branch -d

    # delete all git branches except master
    git branch | egrep -v ^master$ | sed 's/^[ *]*//' | sed 's/^/git branch -D /' | bash



    delete branches for already merged branches into the current branch
    git branch -d about2

    disregard if the branch was merged or not
    git branch -D about2


    """
    return None


def rename_branch():
    """

    git branch -m contact contacts

    """


def update_from_remote_branch():
    """
    git fetch origin master

    git fetch does not touch your working tree at all, so gives you a little breathing space to
    decide what you want to do next.

    then
    git checkout master
    git merge origin/master

    OR use pull that does fetch & merge

    git pull origin master

    """

    return None


def update_remote_branch():
    """
    git push origin branch_to_be_updated
    git push origin dec15_test_do_cones_01
    git push origin new_fitness_func_test_v01
    whenever I create a new branch that I also want on a remote,
    the first command I run after checking out that new branch is
    git push -u [remote name] [branch name]
    Not only does that set the upstream for the branch, but if that
    branch name doesn't exist on the remote, Git creates it.
    -u
    --set-upstream
    For every branch that is up to date or successfully pushed, add upstream (tracking) reference,
    used by argument-less git-pull[1] and other commands. For more information, see branch.<name>.merge in git-config[1].
    """

    return None

