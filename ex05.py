import os
import sys

from joblib import Parallel, delayed

import time

parallel_run = int(sys.argv[1])


def task1(i):
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo Sample {i}, Task1")


def task2(i):
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo Sample {i}, Task2")


def task3(i):
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo Sample {i}, Task3")


def task4(i):
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo Sample {i}, Task4")


def task5(i):
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo Sample {i}, Task5")


def task6(i):
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo Sample {i}, Task6")


def task7(i):
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo Sample {i}, Task7")


def task8(i):
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo Sample {i}, Task8")


def alltask1():
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo All Task 1")


def alltask2():
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo All Task 2")


def alltask3():
    os.system(f"ping 127.0.0.1 -n 2 > nul && echo All Task 3")


def ptask(sample_index):

    cmd = "C:\\Python\\3.6.1\\python.exe "
    cmd += " ".join(sys.argv[:-1]) + f" {sample_index}"
    os.system(cmd)


def parallel_task():
    global parallel_run
    if parallel_run != 1000:
        return

    Parallel(n_jobs=6)(delayed(ptask)(i) for i in range(6))


pipeline = [alltask1,
             alltask2,
             parallel_task,
             [task1,
              task2,
              task3,
              task4,
              task5,
              task6,
              task7,
              task8],
            alltask3]


start = time.time()

for task in pipeline:
    if type(task) is list:
        sample_list = []
        if parallel_run > 1000:
            sample_list = range(6)
        if parallel_run < 1000:
            sample_list = [parallel_run]
        for sample in sample_list:
            print()
            print(f"Starting sample {sample}")
            for subtask in task:
                subtask(sample)
            print(f"Ending sample {sample}")
    else:
        if parallel_run < 1000:
            continue
        task()


if parallel_run >= 1000:
    end = time.time()
    print(f"Runtime of the program is {end - start:0.2f}s")